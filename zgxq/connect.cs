﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zgxq
{
    class connect
    {
        public NetworkStream sockStream;
        private TcpClient tcpClient;

        public connect (string nickName)
        {
            string ip = "127.0.0.1"; //"113.6.251.114";
            //ip = "127.0.0.1";
            int nPort = 57250;
            TcpClient tcpClient = new TcpClient();
            IPAddress srvAddr = IPAddress.Parse(ip);
            //连接到服务器  
            try
            {
                tcpClient.Connect(srvAddr, nPort);
            }
            catch (SocketException e)
            {
                MessageBox.Show("无法连接，服务器可能飞去火星了");
                tcpClient.Close();
                throw e;
            }

            NetworkStream sockStream = tcpClient.GetStream();
            string sendMsg = "+"+ nickName;
            Byte[] buf = Encoding.UTF8.GetBytes(sendMsg);

            try
            {
                sockStream.Write(buf, 0, buf.Length);//发送连接请求
                sockStream.Flush();
                sockStream.ReadTimeout = 5000;
                sockStream.Read(buf, 0, 1); //如果服务器返回结果说过说明连接成功，否则抛出IO异常
                if (buf[0] != 'Y') throw new IOException(); //如果不能返回Y，也需要抛出异常
            }
            catch (SocketException e)
            {
                MessageBox.Show("服务器响应出错");
                sockStream.Close();
                tcpClient.Close();
                throw e;
            }
            catch (IOException e)
            {
                MessageBox.Show("服务器未响应或者昵称id已经存在，无法连接");
                sockStream.Close();
                tcpClient.Close();
                throw e;
            }
            this.sockStream = sockStream;
            this.tcpClient = tcpClient;
            //Thread th = new Thread(new ThreadStart(daemon));
            //th.IsBackground = false;
            //th.Start();
        }

        public void daemon() //
        {

        }
    }
}
