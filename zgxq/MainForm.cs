﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zgxq
{
    public partial class MainForm : Form
    {
        static public MyButton[,] button = new MyButton[10,9];
        connect con;
        static public int color;
        static public int whoGo;//1表示自己走，2表示对方走
        public MainForm()
        {
            InitializeComponent();
            int ClientW = 500, ClientH = 500;
            
            this.panel.Location = new System.Drawing.Point(ClientW - 40  ,10);
            
            this.ClientSize = new System.Drawing.Size(ClientW + 120, ClientH);
            int btnW = 50, btnH = 50;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    MainForm.button[i, j] = new MyButton();
                    
                    MainForm.button[i, j].i = i;
                    MainForm.button[i, j].j = j;

                    MainForm.button[i, j].tt = 2;
                    if (i >= 5)
                    {
                        MainForm.button[i, j].tt = 1;
                    }
                    MainForm.button[i, j].setUnvisible();
                    MainForm.button[i, j].Location = new System.Drawing.Point(j * btnW, i * btnH);
                    MainForm.button[i, j].Name = (i * 10 + j).ToString();
                    MainForm.button[i, j].Size = new System.Drawing.Size(btnW - 5, btnH - 5);
                    //this.button[i, j].TabIndex = i + j;
                    //this.button[i, j].Text = i.ToString()+j.ToString();
                    MainForm.button[i, j].Text = "";
                    MainForm.button[i, j].UseVisualStyleBackColor = true;
                    MainForm.button[i, j].Click += button_Click;
                    this.Controls.Add(MainForm.button[i, j]);
                    //this.button[i, j].Text = "象";

                    //this.button[i, j].Enabled = false;
                }
            }
            //MainForm.button[0, 0].setVisible("车");
            MainForm.playing = false;
        }
        

        public bool Move(int si,int sj,int ei,int ej)
        {
            if (MainForm.button[si, sj].vis == false || MainForm.button[si, sj].tt == 2) return false;
            //if (this.button[ei, ej].vis == true) return false;
            bool success = false;
            if (MainForm.button[si, sj].Text == "卒" || MainForm.button[si, sj].Text == "兵")
            {
                if (si >= 5)
                {
                    if (ej == sj && ei == si - 1) success = true; //向前
                }
                else
                {
                    if (ej == sj && ei == si - 1) success = true;  //向前
                    else if (ej == sj-1 && ei == si) success = true; //向左
                    else if (ej == sj+1 && ei == si) success = true; //向右
                }
            }
            else if (MainForm.button[si, sj].Text == "车" || MainForm.button[si, sj].Text == "車" || MainForm.button[si, sj].Text == "炮" )
            {
                if (si == ei) //横着走
                {
                    int minj = sj > ej ? ej : sj;
                    int maxj = sj > ej ? sj : ej;
                    for(int j=minj+1;j<maxj;j++)
                    {
                        if (MainForm.button[si, j].vis == true) return false;
                    }
                    success = true;
                }
                else if (sj == ej) //横着走
                {
                    int mini = si > ei ? ei : si;
                    int maxi = si > ei ? si : ei;
                    for (int i = mini + 1; i < maxi; i++)
                    {
                        if (MainForm.button[i, sj].vis == true) return false;
                    }
                    success = true;
                }
            }
            else if (MainForm.button[si, sj].Text == "马")
            {
                if (ei == si - 2 && ej == sj - 1 && MainForm.button[si-1, sj].vis == false) success = true;
                else if (ei == si - 1 && ej == sj - 2 && MainForm.button[si, sj-1].vis == false) success = true;
                else if (ei == si + 1 && ej == sj - 2 && MainForm.button[si, sj-1].vis == false) success = true;
                else if (ei == si + 2 && ej == sj - 1 && MainForm.button[si+1, sj].vis == false) success = true;
                else if (ei == si + 2 && ej == sj + 1 && MainForm.button[si+1, sj].vis == false) success = true;
                else if (ei == si + 1 && ej == sj + 2 && MainForm.button[si, sj+1].vis == false) success = true;
                else if (ei == si - 1 && ej == sj + 2 && MainForm.button[si, sj+1].vis == false) success = true;
                else if (ei == si - 2 && ej == sj + 1 && MainForm.button[si-1, sj].vis == false) success = true;
            }
            else if (MainForm.button[si, sj].Text == "象")
            {
                if (ei == si - 2 && ej == sj - 2 && MainForm.button[si - 1, sj-1].vis == false) success = true;
                else if (ei == si - 2 && ej == sj + 2 && MainForm.button[si-1, sj+1].vis == false) success = true;
                else if (ei == si + 2 && ej == sj - 2 && MainForm.button[si+1, sj - 1].vis == false) success = true;
                else if (ei == si + 2 && ej == sj + 2 && MainForm.button[si + 1, sj+1].vis == false) success = true;
            }
            else if (MainForm.button[si, sj].Text == "士")
            {
                if (ei <= 6 || ej<=2 || ej>=6) return false;
                if (ei == si - 1 && ej == sj - 1) success = true;
                else if (ei == si - 1 && ej == sj + 1) success = true;
                else if (ei == si + 1 && ej == sj - 1) success = true;
                else if (ei == si + 1 && ej == sj + 1) success = true;
            }
            else if (MainForm.button[si, sj].Text == "帅" || MainForm.button[si, sj].Text == "将")
            {
                if (ei <= 6 || ej <= 2 || ej >= 6) return false;
                if (ei == si && ej == sj - 1) success = true;
                else if (ei == si && ej == sj + 1) success = true;
                else if (ei == si + 1 && ej == sj) success = true;
                else if (ei == si - 1 && ej == sj) success = true;
            }
            if (success)
            {
                MainForm.button[ei, ej].tt = 1;
                MainForm.button[ei, ej].setVisible(MainForm.button[si, sj].Text);
                MainForm.button[si, sj].setUnvisible();
            }

            return success;
        }
        public bool Eat(int si, int sj, int ei, int ej)
        {
            if (MainForm.button[si, sj].vis == false || MainForm.button[si, sj].tt == 2) return false;
            //if (this.button[ei, ej].vis == true) return false;
            bool success = false;
            if (MainForm.button[si, sj].Text == "炮")
            {
                int sum = 0;
                if (si == ei) //横着走
                {
                    int minj = sj > ej ? ej : sj;
                    int maxj = sj > ej ? sj : ej;
                    for (int j = minj + 1; j < maxj; j++)
                    {
                        if (MainForm.button[si, j].vis == true) sum++;
                    }
                    if (sum == 1)
                        success = true;
                }
                else if (sj == ej) //横着走
                {
                    int mini = si > ei ? ei : si;
                    int maxi = si > ei ? si : ei;
                    for (int i = mini + 1; i < maxi; i++)
                    {
                        if (MainForm.button[i, sj].vis == true) sum++;
                    }
                    if (sum == 1)
                        success = true;
                }
            }
            else return Move(si, sj, ei, ej);
            if (success)
            {
                MainForm.button[ei, ej].tt = 1;
                MainForm.button[ei, ej].setVisible(MainForm.button[si, sj].Text);
                MainForm.button[si, sj].setUnvisible();
            }

            return success;
        }

        int selI=-1, selJ=-1;
        private static bool playing;
        private static int lose; //0表示还没有结果，1表示赢了，2表示输了

        public void button_Click(object sender, EventArgs e)
        {
            if(MainForm.playing == false)
            {
                MessageBox.Show("游戏尚未开始");
                return;
            }
            if (MainForm.whoGo == 2)
            {
                MessageBox.Show("等待对方移子");
                return;
            }
            //MessageBox.Show("NIHAO");
            MyButton btn = (MyButton)sender;
            if(btn.vis==true)  //点的是一个棋子
            {
                if (selI == -1) //还没有选择棋子
                {
                    if (btn.tt == 1)  //选择的是己方棋子
                    {
                        selI = btn.i; //选中当前棋子
                        selJ = btn.j;
                    }
                    else return;
                }
                else //之前已经选中某个棋子
                {
                    if(btn.tt==2) //试图吃掉敌方棋子
                    {
                        bool tmp = false;
                        if (btn.Text == "将" || btn.Text == "帅") tmp = true;
                        if(Eat(selI, selJ, btn.i, btn.j))
                        {
                            sendMove(selI, selJ, btn.i, btn.j);
                            if(tmp)
                            {
                                MainForm.lose = 1;
                                MessageBox.Show("你赢了");
                            }
                        }
                        //不管是否成功，都要清空选择的棋子
                        selI = -1;
                        selJ = -1;
                    }
                    else if(btn.tt==1) //复选己方棋子
                    {
                        selI = btn.i; //选中当前棋子
                        selJ = btn.j;
                    }
                }
            }
            else  //点的是一个空白区域
            {
                if (selI!=-1&&selJ!=-1) //移动到空白位置
                {
                    if (Move(selI, selJ, btn.i, btn.j))
                    {
                        sendMove(selI, selJ, btn.i, btn.j);
                    }
                    //不管移动是否成功，都要清空选择的棋子
                    selI = -1;
                    selJ = -1;
                }
            }
        }


        public void sendMove(int si,int sj,int ei,int ej)
        {
            this.con.sockStream.Write(Encoding.UTF8.GetBytes("M" + si + sj + ei + ej), 0, 5);
            MainForm.whoGo = 2;
            Thread th = new Thread(new ThreadStart(bkWaitToMove));
            th.IsBackground = true;
            th.Start();
        }

        public void bkWaitToMove()
        {
            Byte[] buf = new Byte[5];
            this.con.sockStream.ReadTimeout = 1000 * 60 * 10;//10分钟
            try
            {
                this.con.sockStream.Flush();
                int len = this.con.sockStream.Read(buf, 0, 5);
                if(len!=5)
                {
                    MessageBox.Show("服务器未能返回正确结果");
                }
                else if (buf[0] == 'E')
                {
                    MessageBox.Show("对方返回错误 code:" + buf[1]);
                }
                else if (buf[0] == 'M')
                {
                    int si = 9-buf[1]+'0';
                    int sj = 8-buf[2]+'0';
                    int ei = 9-buf[3]+'0';
                    int ej = 8-buf[4]+'0';
                    if (MainForm.button[ei, ej].Text == "将" || MainForm.button[ei, ej].Text == "帅") MainForm.lose = 2;
                    MainForm.button[ei, ej].tt = 2;
                    MainForm.button[ei, ej].setVisible(MainForm.button[si, sj].Text);
                    MainForm.button[si, sj].setUnvisible();
                    if (MainForm.lose==2)
                    {
                        MessageBox.Show("你输了！");
                    }
                    MainForm.whoGo = 1;
                }
                else
                {
                    MessageBox.Show("意外错误bkWaitToMove "+ Encoding.UTF8.GetString(buf));
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("连接超时，对方无应答");
            }
            finally
            {
                this.con.sockStream.ReadTimeout = 5000;
            }
        }



        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (this.nickName.Text=="" || this.nickName.Text.Contains(","))
            {
                MessageBox.Show("昵称不合法：不能为空且不能包含逗号字符(,)");
                return;
            }
            this.btnConnect.Enabled = false;
            this.nickName.ReadOnly = true;
            try
            {
                this.con = new connect(this.nickName.Text);
                //如果成功创建对象，则继续
                this.btnConnect.Enabled = false;
                this.nickName.ReadOnly = true;
                
                this.btnPlay.Enabled = true;
                this.btnRefreshPlayerList.Enabled = true;
                this.btnRefreshPlayerList_Click(new object(),new EventArgs());
                MessageBox.Show("连接成功，欢迎你！");
                
                return;
            }
            catch
            {
                this.btnConnect.Enabled = true;
                this.nickName.ReadOnly = false;
            }
            
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            try
            {
                this.con.sockStream.Write(Encoding.ASCII.GetBytes("-"), 0, 1);
                this.con = null;
            }
            catch { }
            finally
            {
                this.nickName.ReadOnly = false;
                this.btnConnect.Enabled = true;

                this.btnPlay.Enabled = false;
                this.btnRefreshPlayerList.Enabled = false;
            }
        }

        private void btnRefreshPlayerList_Click(object sender, EventArgs e)
        {
            this.playerList.Items.Clear();
            try
            {
                this.con.sockStream.Write(Encoding.UTF8.GetBytes("L"), 0, 1);
                Byte[] buf = new Byte[128*1000];
                this.con.sockStream.Read(buf, 0, 128 * 1000);
                string str = Encoding.UTF8.GetString(buf);
                //MessageBox.Show(playerList);
                string[] playerList = str.Split(',');
                for(int i=0;i<playerList.Length;i++)
                    if(playerList[i]!="")
                        this.playerList.Items.Add(playerList[i]);
                if (this.playerList.Text == "")
                {
                    this.playerList.Text = this.playerList.Items[0].ToString();
                }
            }
            catch
            {
                MessageBox.Show("出错了哦");
            }
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            try
            {
                Byte[] buf = Encoding.UTF8.GetBytes("P" + this.playerList.Text);
                this.con.sockStream.Write(buf,0,buf.Length);
                buf[0] = 0;
                this.con.sockStream.Read(buf, 0, 2);
                if(buf[0]=='W') //成功发起请求
                {
                    this.btnPlay.Enabled = false;
                    this.playerList.Enabled = false;
                    this.btnDisconnect.Enabled = false;
                    this.btnRefreshPlayerList.Enabled = false;
                    MainForm.whoGo = 2;
                    MainForm.color = 1;
                    ready();

                    Thread th = new Thread(new ThreadStart(bkWaitToStart));
                    th.IsBackground = true;
                    th.Start();
                }
                else if (buf[0] == 'S') //立即开始
                {
                    this.btnPlay.Enabled = false;
                    this.playerList.Enabled = false;
                    this.btnDisconnect.Enabled = false;
                    this.btnRefreshPlayerList.Enabled = false;
                    MainForm.color = 2;
                    MainForm.whoGo = 2;
                    ready();
                    Thread th = new Thread(new ThreadStart(bkWaitToMove));
                    th.IsBackground = true;
                    th.Start();
                }
                else if(buf[0]=='E') //出现错误
                {
                    if(buf[1]=='0')
                        MessageBox.Show("不能与自己对局");
                    else if (buf[1] == '1')
                        MessageBox.Show("对方已下线");
                    else if (buf[1] == '2')
                        MessageBox.Show("对方在和其他用户玩耍，哈哈");
                }
                else
                {
                    MessageBox.Show("系统错误，请联系开发人员");
                    return;
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("连接超时");
            }
            catch(SocketException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void bkWaitToStart()
        {
            Byte[] buf = new Byte[2];
            this.con.sockStream.ReadTimeout = 30000;//30秒
            try
            {
                this.con.sockStream.Flush();
                this.con.sockStream.Read(buf, 0, 2);
                if(buf[0]=='E')
                {
                    MessageBox.Show("对方返回错误 code:"+buf[1]);
                }
                else if(buf[0]=='S')
                {
                    //MessageBox.Show("成功发起对局");
                    MainForm.color = 1;
                    
                    MainForm.whoGo = 1;
                }
                else
                {
                    MessageBox.Show("意外错误");
                }
            }
            catch(IOException ex)
            {
                MessageBox.Show("连接超时，对方无应答");
            }
            finally
            {
                this.con.sockStream.ReadTimeout = 5000;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.btnPlay.Enabled = false;
            this.btnRefreshPlayerList.Enabled = false;
        }

        public void ready()  //color=1表示红方，color=2表示黑方
        {
            MainForm.lose = 0;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    MainForm.button[i, j].tt = 2;
                    if (i >= 5)
                    {
                        MainForm.button[i, j].tt = 1;
                    }
                    MainForm.button[i, j].setUnvisible();
                    MainForm.button[i, j].Text = "";
                    MainForm.button[i, j].UseVisualStyleBackColor = true;
                }
            }
            MainForm.button[0, 0].setVisible("车");
            MainForm.button[0, 1].setVisible("马");
            MainForm.button[0, 8].setVisible("车");
            MainForm.button[0, 7].setVisible("马");
            MainForm.button[0, 2].setVisible("象");
            MainForm.button[0, 3].setVisible("士");
            MainForm.button[0, 4].setVisible("将");
            MainForm.button[0, 5].setVisible("士");
            MainForm.button[0, 6].setVisible("象");

            MainForm.button[2, 1].setVisible("炮");
            MainForm.button[2, 7].setVisible("炮");

            MainForm.button[3, 0].setVisible("卒");
            MainForm.button[3, 2].setVisible("卒");
            MainForm.button[3, 4].setVisible("卒");
            MainForm.button[3, 6].setVisible("卒");
            MainForm.button[3, 8].setVisible("卒");



            MainForm.button[9, 0].setVisible("车");
            MainForm.button[9, 1].setVisible("马");
            MainForm.button[9, 8].setVisible("车");
            MainForm.button[9, 7].setVisible("马");
            MainForm.button[9, 2].setVisible("象");
            MainForm.button[9, 3].setVisible("士");
            MainForm.button[9, 4].setVisible("将");
            MainForm.button[9, 5].setVisible("士");
            MainForm.button[9, 6].setVisible("象");

            MainForm.button[7, 1].setVisible("炮");
            MainForm.button[7, 7].setVisible("炮");

            MainForm.button[6, 0].setVisible("兵");
            MainForm.button[6, 2].setVisible("兵");
            MainForm.button[6, 4].setVisible("兵");
            MainForm.button[6, 6].setVisible("兵");
            MainForm.button[6, 8].setVisible("兵");
            MainForm.playing = true;
        }
    }

    public class MyButton : Button
    {
        public int tt; //1表示己方，2表示敌方
        public MyButton()
        {
            this.rFlatStyle = this.FlatStyle;//样式  

            this.FlatAppearance.BorderSize = 0;
            this.TabStop = false;
        }
        public void setUnvisible()
        {
            this.Text = "";
            this.FlatStyle = FlatStyle.Flat;//样式  
            this.vis = false;
        }
        static public void setUnvisibleS(int i,int j)
        {
            MainForm.button[i, j].Text = "";
            MainForm.button[i, j].FlatStyle = FlatStyle.Flat;//样式  
            MainForm.button[i, j].vis = false;
        }
        public void setVisible(string txt = "")
        {
            if (this.tt == 1) this.ForeColor = MainForm.color == 1 ? Color.Red : Color.Black;
            else if (this.tt == 2) this.ForeColor = MainForm.color == 2 ? Color.Red : Color.Black;
            this.Text = txt;
            this.FlatStyle = this.rFlatStyle;//样式  
            this.vis = true;
        }
        static public void setVisibleS(int i,int j,string txt="")
        {
            if(MainForm.button[i,j].tt==1) MainForm.button[i, j].ForeColor = MainForm.color==1?Color.Red: Color.Black;
            else if (MainForm.button[i, j].tt == 2) MainForm.button[i, j].ForeColor = MainForm.color == 2 ? Color.Red : Color.Black;
            MainForm.button[i, j].Text = txt;
            MainForm.button[i, j].FlatStyle = MainForm.button[i, j].rFlatStyle;//样式  
            MainForm.button[i, j].vis = true;
        }
        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            base.OnPaint(e);
            System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
            path.AddEllipse(0, 0, this.Width, this.Height);
            this.Region = new Region(path);
        }
        protected override void OnMouseEnter(EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.DrawEllipse(new Pen(Color.Blue), 0, 0, this.Width, this.Height);
            g.Dispose();
        }
        public int i, j;
        public bool vis = true;


        private readonly FlatStyle rFlatStyle;
    }
}
