﻿namespace zgxq
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel = new System.Windows.Forms.Panel();
            this.btnRefreshPlayerList = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.playerList = new System.Windows.Forms.ComboBox();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.nickName = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.btnRefreshPlayerList);
            this.panel.Controls.Add(this.btnPlay);
            this.panel.Controls.Add(this.playerList);
            this.panel.Controls.Add(this.btnDisconnect);
            this.panel.Controls.Add(this.label1);
            this.panel.Controls.Add(this.nickName);
            this.panel.Controls.Add(this.btnConnect);
            this.panel.Location = new System.Drawing.Point(61, 44);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(194, 435);
            this.panel.TabIndex = 0;
            // 
            // btnRefreshPlayerList
            // 
            this.btnRefreshPlayerList.Location = new System.Drawing.Point(18, 220);
            this.btnRefreshPlayerList.Name = "btnRefreshPlayerList";
            this.btnRefreshPlayerList.Size = new System.Drawing.Size(161, 31);
            this.btnRefreshPlayerList.TabIndex = 11;
            this.btnRefreshPlayerList.Text = "刷新玩家列表";
            this.btnRefreshPlayerList.UseVisualStyleBackColor = true;
            this.btnRefreshPlayerList.Click += new System.EventHandler(this.btnRefreshPlayerList_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.Location = new System.Drawing.Point(18, 257);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(161, 29);
            this.btnPlay.TabIndex = 10;
            this.btnPlay.Text = "发起对局";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // playerList
            // 
            this.playerList.FormattingEnabled = true;
            this.playerList.Location = new System.Drawing.Point(18, 190);
            this.playerList.Name = "playerList";
            this.playerList.Size = new System.Drawing.Size(161, 23);
            this.playerList.TabIndex = 9;
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Location = new System.Drawing.Point(18, 100);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(161, 28);
            this.btnDisconnect.TabIndex = 8;
            this.btnDisconnect.Text = "断开连接";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "昵称id：";
            // 
            // nickName
            // 
            this.nickName.Location = new System.Drawing.Point(18, 38);
            this.nickName.Name = "nickName";
            this.nickName.Size = new System.Drawing.Size(161, 25);
            this.nickName.TabIndex = 6;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(18, 68);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(161, 29);
            this.btnConnect.TabIndex = 5;
            this.btnConnect.Text = "连接";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 692);
            this.Controls.Add(this.panel);
            this.Name = "MainForm";
            this.Text = "中国象棋";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label IPself;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.TextBox nickName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Button btnRefreshPlayerList;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.ComboBox playerList;
    }
}

