﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zgxqServer
{
    public partial class MainForm : Form
    {
        public static Dictionary<string,player> players = new Dictionary<string,player>();

        public MainForm()
        {
            InitializeComponent();
            Thread th = new Thread(new ThreadStart(startListen));
            th.IsBackground = true;
            th.Start();

            Thread th2 = new Thread(new ThreadStart(daemon));
            th2.IsBackground = true;
            th2.Start();
        }

        public void daemon()
        {
            int time = 300000;//30s
            while (true)
            {
                
                System.Threading.Thread.Sleep(time);//每隔30秒钟检查
                time = 300000;
                string tt = "";
                foreach (KeyValuePair<string, player> kv in MainForm.players)
                {
                    if (kv.Value.nickName == "")
                    {
                        tt = kv.Key;
                        time = 0;
                        break;
                    }
                }
                if(time==0)
                    MainForm.players.Remove(tt);
            }
        }


        public void startListen() //被动连接
        {
            int nPort = 57250;
            string srvIP = "127.0.0.1"; //"113.6.251.114";
            //srvIP = "127.0.0.1";
            IPAddress srvAddr = IPAddress.Parse(srvIP);
            //创建并绑定套接字  
            TcpListener srvLstner = new TcpListener(srvAddr, nPort);

            //开始监听  
            try
            {
                srvLstner.Start(5);
            }
            catch (SocketException e)
            {
                MessageBox.Show(e.Message);
                //Console.WriteLine(e.Message);
                srvLstner.Stop();
                //Console.ReadLine();
                return;
            }
            //接受客户端连接请求  
            while (true)
            {
                //接收连接请求  
                Socket remoteSock = srvLstner.AcceptSocket();

                //显示客户端发送过来的消息  
                Byte[] recvBuf = new Byte[128];
                try
                {
                    remoteSock.Receive(recvBuf);
                }
                catch (SocketException e)
                {
                    MessageBox.Show(e.Message);
                    break;
                }
                string recvMsg = Encoding.UTF8.GetString(recvBuf);
                if (recvMsg[0] == '+')
                {
                    int len = 0;
                    while (recvBuf[len] != 0) len++;
                    string nickName = recvMsg.Substring(1, len-1);
                    string ip = remoteSock.RemoteEndPoint.ToString();
                    if (MainForm.players.ContainsKey(nickName))
                    {
                        if (MainForm.players[nickName].nickName == "")
                        {
                            MainForm.players[nickName] = new player(nickName, ip, remoteSock);
                            remoteSock.Send(Encoding.ASCII.GetBytes("Y"));
                        }
                    }
                    else {
                        MainForm.players.Add(nickName, new player(nickName, ip, remoteSock));
                        remoteSock.Send(Encoding.ASCII.GetBytes("Y"));
                    }
                }
            }
            srvLstner.Stop();
        }
        

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void refreshPlayers_Click(object sender, EventArgs e)
        {
            this.playersList.Rows.Clear();
            foreach (KeyValuePair<string,player> kv in MainForm.players)
            {
                if(kv.Value.nickName=="")
                {
                    continue;
                }
                int id = this.playersList.Rows.Add();
                this.playersList.Rows[id].Cells[0].Value = kv.Value.nickName;
                this.playersList.Rows[id].Cells[1].Value = kv.Value.ip;
            }
        }

        private void sendBtn_Click(object sender, EventArgs e)
        {
        }
    }
}
