﻿namespace zgxqServer
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.refreshPlayers = new System.Windows.Forms.Button();
            this.sendBtn = new System.Windows.Forms.Button();
            this.playersList = new System.Windows.Forms.DataGridView();
            this.nickName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.playerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            MainForm.log = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.playersList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "所有在线玩家：";
            // 
            // refreshPlayers
            // 
            this.refreshPlayers.Location = new System.Drawing.Point(24, 43);
            this.refreshPlayers.Name = "refreshPlayers";
            this.refreshPlayers.Size = new System.Drawing.Size(109, 30);
            this.refreshPlayers.TabIndex = 2;
            this.refreshPlayers.Text = "刷新";
            this.refreshPlayers.UseVisualStyleBackColor = true;
            this.refreshPlayers.Click += new System.EventHandler(this.refreshPlayers_Click);
            // 
            // sendBtn
            // 
            this.sendBtn.Location = new System.Drawing.Point(24, 95);
            this.sendBtn.Name = "sendBtn";
            this.sendBtn.Size = new System.Drawing.Size(109, 29);
            this.sendBtn.TabIndex = 3;
            this.sendBtn.Text = "发送数据";
            this.sendBtn.UseVisualStyleBackColor = true;
            this.sendBtn.Click += new System.EventHandler(this.sendBtn_Click);
            // 
            // playersList
            // 
            this.playersList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.playersList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nickName,
            this.ip});
            this.playersList.Location = new System.Drawing.Point(159, 43);
            this.playersList.Name = "playersList";
            this.playersList.RowTemplate.Height = 27;
            this.playersList.Size = new System.Drawing.Size(440, 286);
            this.playersList.TabIndex = 4;
            // 
            // nickName
            // 
            this.nickName.HeaderText = "昵称";
            this.nickName.Name = "nickName";
            // 
            // ip
            // 
            this.ip.HeaderText = "IP";
            this.ip.Name = "ip";
            // 
            // playerBindingSource
            // 
            this.playerBindingSource.DataSource = typeof(zgxqServer.player);
            // 
            // log
            // 
            MainForm.log.AutoSize = true;
            MainForm.log.Location = new System.Drawing.Point(21, 372);
            MainForm.log.Name = "log";
            MainForm.log.Size = new System.Drawing.Size(31, 15);
            MainForm.log.TabIndex = 5;
            MainForm.log.Text = "log";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 612);
            this.Controls.Add(MainForm.log);
            this.Controls.Add(this.playersList);
            this.Controls.Add(this.sendBtn);
            this.Controls.Add(this.refreshPlayers);
            this.Controls.Add(this.label1);
            this.Name = "MainForm";
            this.Text = "中国象棋服务器端";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.playersList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button refreshPlayers;
        private System.Windows.Forms.Button sendBtn;
        private System.Windows.Forms.DataGridView playersList;
        private System.Windows.Forms.DataGridViewTextBoxColumn nickName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ip;
        private System.Windows.Forms.BindingSource playerBindingSource;
        static public System.Windows.Forms.Label log;
    }
}

