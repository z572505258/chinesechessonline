﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zgxqServer
{
    public class player
    {
        public string nickName;
        public string ip;
        public string playWith;
        public int updTime;
        public Socket socket;
        public player(string name,string ip, Socket remoteSock)
        {
            this.playWith = "";
            this.nickName = name;
            this.ip = ip;
            System.DateTime tt = new System.DateTime();
            this.updTime = tt.Second;
            this.socket = remoteSock;
            Thread th = new Thread(new ParameterizedThreadStart(daemon));
            th.IsBackground = true;
            th.Start();
        }
        public void daemon(object obj)
        {
            try
            {
                Byte[] buf = new Byte[128];
                while (true)
                {
                    this.socket.ReceiveTimeout = 1000 * 60 * 20; //20分钟内如果没有数据交互，则视为客户端断开连接，如果客户主动断开或者强制关闭客户端都会导致立即断开
                    try
                    {
                        this.socket.Receive(buf);
                    }
                    catch
                    {
                        this.nickName = "";
                        return;
                    }
                    if (buf[0] == '-') //断开连接
                    {
                        this.nickName = "";
                        return;
                    }
                    else if (buf[0] == 'L') //请求玩家列表
                    {
                        //MessageBox.Show(this.nickName + "请求玩家列表");
                        string tt = "";
                        foreach (KeyValuePair<string, player> kv in MainForm.players)
                        {
                            if (kv.Value.nickName == "")
                            {
                                continue;
                            }
                            tt += kv.Key + ",";
                        }
                        this.socket.Send(Encoding.UTF8.GetBytes(tt));
                    }
                    else if (buf[0] == 'P') //要和某玩家匹配
                    {
                        string nickName = Encoding.UTF8.GetString(buf);
                        int len = 1;
                        while (buf[len] != 0) len++;
                        nickName = nickName.Substring(1, len - 1);
                        if (nickName == this.nickName)
                        {
                            this.socket.Send(Encoding.UTF8.GetBytes("E0")); //不能与自己对局
                            continue;
                        }
                        else if (!MainForm.players.ContainsKey(nickName) || MainForm.players[nickName].nickName == "")
                        {
                            this.socket.Send(Encoding.UTF8.GetBytes("E1")); //对方已经下线，哈哈
                            continue;
                        }
                        else if (MainForm.players[nickName].playWith != "" && MainForm.players[nickName].playWith != this.nickName)
                        { //人家在和别人玩，哈哈，不跟你玩
                            this.socket.Send(Encoding.UTF8.GetBytes("E2"));
                            continue;
                        }
                        else if (MainForm.players[nickName].playWith == "")
                        {
                            this.socket.Send(Encoding.UTF8.GetBytes("W"));
                            this.playWith = nickName;
                            try
                            {
                                int maxT = 0;
                                while (MainForm.players[nickName].playWith == "" && maxT <= 30) //30秒后断开
                                {
                                    maxT++;
                                    System.Threading.Thread.Sleep(1000); //每隔一秒检查对方是否也请求与该玩家匹配
                                }
                            }
                            catch
                            { //中途对方可能意外断开
                                this.socket.Send(Encoding.UTF8.GetBytes("E4"));
                                this.playWith = "";
                                continue;
                            }
                        }
                        if (MainForm.players[nickName].playWith == this.nickName)
                        {
                            //MessageBox.Show("kaishi");
                            this.socket.Send(Encoding.UTF8.GetBytes("S"));
                            this.playWith = nickName;
                        }
                        else
                        {
                            this.socket.Send(Encoding.UTF8.GetBytes("E5"));
                            this.playWith = "";
                        }
                    }
                    else if(buf[0]=='M')
                    {
                        Byte[] bufT = new Byte[5];
                        for (int i = 0; i < 5; i++) bufT[i] = buf[i];
                        //MainForm.log.Text += Encoding.UTF8.GetString(bufT);
                        MainForm.players[playWith].socket.Send(bufT);
                    }
                }
            }
            catch
            {
                this.nickName = "";
                return;
            }
        }
        public void send(string msg)
        {
            this.socket.Send(Encoding.ASCII.GetBytes(msg));
        }
        
    }
}
